﻿using StudentScoreManageCommon;
using StudentScoreManageDAL;
using StudentScoreManageEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;

namespace StudentScoreManageBLL
{
    public class UserInfoService
    {
        private UserInfoDAL userInfoDAL = new UserInfoDAL();
        public UserInfo GetUserByUserName(string userName)
        {
            var userInfo = userInfoDAL.GetUserByUserName(userName);
            return userInfo;
        }
    }
}
