﻿using StudentScoreManageDAL;
using StudentScoreManageEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentScoreManageBLL
{
    
    public class ManageService
    {
        private ManageDAL managerDAL = new ManageDAL();


        #region 取得课程数据
        public DataTable GetCourseList()
        {
            return managerDAL.GetCourseList();
        }
        #endregion

        public int ExeSql(string sql)
        {
            return managerDAL.ExeSql(sql);
        }
        /// <summary>
        /// 获取用户班级成绩信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetGradeList()
        {
            return managerDAL.GetGradeList();
        }

        /// <summary>
        /// 获取班级信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetClassName()
        {
            return managerDAL.GetClassName();
        }

        /// <summary>
        /// 根据班级名称获取学生信息
        /// </summary>
        /// <param name="className"></param>
        /// <returns></returns>
        public DataTable GetStudentByClassName(string className)
        {
            return managerDAL.GetStudentByClassName(className);
        }
        public DataTable GetAdminUrl()
        {
            return managerDAL.GetAdminUrl();
        }
        public DataTable GetGroupList()
        {
            return managerDAL.GetGroupList();
        }
        public DataTable GetStudent()
        {
            return managerDAL.GetStudent();
        }
        public DataTable GetSearchStudent(StudentInfo model)
        {
            return managerDAL.GetSearchStudent(model);
        }

        public DataTable GetStudentStatis()
        {
            return managerDAL.GetStudentStatis();
        }

        public DataTable GetTeacherCourse()
        {
            return managerDAL.GetTeacherCourse();
        }
    }
}
