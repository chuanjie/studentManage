﻿using StudentScoreManageBLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StudentScoreManageSite
{
    public partial class AdminCourse : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.DataGrid DataGrid1;
        protected System.Web.UI.WebControls.Button Button2;

        private void Page_Load(object sender, System.EventArgs e)
        {
            // 在此处放置用户代码以初始化页面
            if (!Page.IsPostBack)
            {
                GetData();
            }
        }
        private void GetData()
        {
            DataTable dt = new ManageService().GetCourseList();
            this.DataGrid1.DataSource = dt;
            this.DataGrid1.DataBind();
        }
        #region Web 窗体设计器生成的代码
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: 该调用是 ASP.NET Web 窗体设计器所必需的。
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// 设计器支持所需的方法 - 不要使用代码编辑器修改
        /// 此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.Button2.Click += new System.EventHandler(this.Button2_Click);
            this.DataGrid1.CancelCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.DataGrid1_CancelCommand);
            this.DataGrid1.EditCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.DataGrid1_EditCommand);
            this.DataGrid1.UpdateCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.DataGrid1_UpdateCommand);
            this.DataGrid1.DeleteCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.DataGrid1_DeleteCommand);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion



        

        private void Button2_Click(object sender, System.EventArgs e)
        {
            Response.Redirect("AdminCourseAdd.aspx");
        }

        private void DataGrid1_EditCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            this.DataGrid1.EditItemIndex = e.Item.ItemIndex;
            GetData();
        }

        private void DataGrid1_DeleteCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            string sid = e.Item.Cells[0].Text;
            string sql = string.Format("delete 课程表 where 课程号 = '{0}'", sid);
            new ManageService().ExeSql(sql);
            this.DataGrid1.EditItemIndex = -1;
            GetData();

        }

        private void DataGrid1_CancelCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            this.DataGrid1.EditItemIndex = -1;
            GetData();

        }

        private void DataGrid1_UpdateCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
        {
            string sid = e.Item.Cells[0].Text;

            TextBox txtName = new TextBox();
            txtName = (TextBox)e.Item.Cells[1].Controls[0];
            string name = txtName.Text.ToString();

            TextBox txtAge = new TextBox();
            txtAge = (TextBox)e.Item.Cells[2].Controls[0];
            string age = txtAge.Text.ToString();


            string sql = string.Empty;
            sql = string.Format("update 课程表 set 课程名='{0}',先修课='{1}' where 课程号='{2}'", name, age, sid);
            new ManageService().ExeSql(sql);
            this.DataGrid1.EditItemIndex = -1;
            GetData();

        }
    }
}