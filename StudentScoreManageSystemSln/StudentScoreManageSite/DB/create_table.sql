use [GradeSys]
go


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[授课表]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[授课表](
	[教师名] [char](10) NULL,
	[课程号] [char](8) NOT NULL,
	[学时数] [tinyint] NULL,
	[班级名] [char](10) NOT NULL,
 CONSTRAINT [PK_授课表] PRIMARY KEY CLUSTERED 
(
	[课程号] ASC,
	[班级名] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[成绩表]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[成绩表](
	[学号] [char](10) NOT NULL,
	[课程号] [char](8) NOT NULL,
	[成绩] [tinyint] NULL,
 CONSTRAINT [PK_成绩表] PRIMARY KEY CLUSTERED 
(
	[学号] ASC,
	[课程号] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Admin]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Admin](
	[userid] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](50) NULL,
	[password] [varchar](50) NULL,
	[group] [char](20) NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AdminUrl]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AdminUrl](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Url] [varchar](50) NOT NULL,
	[UrlName] [varchar](50) NOT NULL,
	[Comment] [varchar](50) NULL,
 CONSTRAINT [PK_AdminUrl] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AdminGroup]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AdminGroup](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Group] [varchar](50) NOT NULL,
	[Promise] [varchar](50) NULL,
	[comment] [varchar](50) NULL,
 CONSTRAINT [PK_AdminGroup] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[Group] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[学生信息表]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[学生信息表](
	[学号] [char](10) NOT NULL,
	[姓名] [char](10) NULL,
	[性别] [char](10) NULL,
	[年龄] [tinyint] NULL,
	[所在院系] [varchar](50) NULL,
	[班级名] [varchar](50) NULL,
	[入学年份] [datetime] NULL,
 CONSTRAINT [PK_学生信息表] PRIMARY KEY CLUSTERED 
(
	[学号] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[课程表]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[课程表](
	[课程号] [char](8) NOT NULL,
	[课程名] [char](20) NULL,
	[先修课] [char](20) NULL,
 CONSTRAINT [PK_课程表] PRIMARY KEY CLUSTERED 
(
	[课程号] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_stuGrade]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[v_stuGrade]
AS
SELECT dbo.成绩表.学号, dbo.成绩表.课程号, dbo.成绩表.成绩, dbo.学生信息表.姓名, 
      dbo.学生信息表.年龄, dbo.学生信息表.班级名, dbo.学生信息表.入学年份
FROM dbo.成绩表 INNER JOIN
      dbo.学生信息表 ON dbo.成绩表.学号 = dbo.学生信息表.学号
' 
