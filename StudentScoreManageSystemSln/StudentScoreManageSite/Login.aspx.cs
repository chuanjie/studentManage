﻿using StudentScoreManageBLL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StudentScoreManageSite
{
    public partial class Login : System.Web.UI.Page
    {
        private SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["GradeSys"]);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                string user = this.txtUsername.Text;
                string pwd = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(this.txtPassword.Text, "MD5");
                var data = new UserInfoService().GetUserByUserName(user);

                if (data != null && data.PassWord == pwd)
                {
                    Session["username"] = data.UserName;
                    Session["group"] = data.group;
                    Response.Redirect("Main.aspx");
                }
                else
                {
                    Response.Write(string.Format("<script>alert('用户名或者密码错误,请重新输入!{0}')</script>",data.PassWord+"="+pwd));
                }
            }
        }

        protected void Button1_Click(object sender, System.EventArgs e)
        {
           
        }


    }
}