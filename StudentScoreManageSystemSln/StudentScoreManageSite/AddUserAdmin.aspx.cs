﻿using StudentScoreManageBLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StudentScoreManageSite
{
    public partial class AddUserAdmin : System.Web.UI.Page
    {
        protected System.Web.UI.WebControls.TextBox txtPassWord;
        protected System.Web.UI.WebControls.TextBox txtUserName;
        protected System.Web.UI.WebControls.Button Button1;
        protected System.Web.UI.WebControls.RequiredFieldValidator RequiredFieldValidator1;

        private void Page_Load(object sender, System.EventArgs e)
        {
            // 在此处放置用户代码以初始化页面
            GetGroupList();
        }

        #region
        private void AddUserAdminInfo()
        {
            string sql = string.Empty;
            string pwd = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(this.txtPassWord.Text, "MD5");
            sql = string.Format("insert into [dbo].[Admin] (username, [password],[group]) values('{0}', '{1}', '{2}') ", this.txtUserName.Text,
                pwd, this.DropDownList1.SelectedItem.Text);
            new ManageService().ExeSql(sql);
            Response.Redirect("AdminGroup.aspx");
        }
        #endregion

        #region
        private void GetGroupList()
        {
            var dt = new ManageService().GetGroupList();
            if (dt != null && dt.Rows.Count > 0)
            {
                this.DropDownList1.Items.Clear();
                foreach (DataRow row in dt.Rows)
                {
                    ListItem ls = new ListItem(row["group"].ToString(), row["id"].ToString());
                    this.DropDownList1.Items.Add(ls);
                }
            }
        }
        #endregion

        #region Web 窗体设计器生成的代码
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: 该调用是 ASP.NET Web 窗体设计器所必需的。
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// 设计器支持所需的方法 - 不要使用代码编辑器修改
        /// 此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        private void Button1_Click(object sender, System.EventArgs e)
        {
            AddUserAdminInfo();

        }
    }
}