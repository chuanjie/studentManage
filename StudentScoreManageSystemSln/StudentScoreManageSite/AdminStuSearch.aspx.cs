﻿using StudentScoreManageBLL;
using StudentScoreManageEntity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace StudentScoreManageSite
{
    public partial class AdminStuSearch : System.Web.UI.Page
    {
        private SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["GradeSys"]);
        protected System.Web.UI.WebControls.Button Button1;
        protected System.Web.UI.WebControls.TextBox txtsid;
        protected System.Web.UI.WebControls.TextBox txtname;
        protected System.Web.UI.WebControls.TextBox txtage;
        protected System.Web.UI.WebControls.TextBox txtx;
        protected System.Web.UI.WebControls.TextBox txty;
        protected System.Web.UI.WebControls.RangeValidator RangeValidator1;
        protected System.Web.UI.WebControls.DataGrid DataGrid1;

        private void Page_Load(object sender, System.EventArgs e)
        {
            // 在此处放置用户代码以初始化页面
            if (!Page.IsPostBack)
            {
                GetStuData();
            }
        }


        #region
        private void GetStuData()
        {
            StudentInfo sInfo = new StudentInfo();
            sInfo.StudentNumber = this.txtsid.Text.ToString();
            sInfo.StudentName = this.txtname.Text.ToString();
            int age = 0;
            int.TryParse(this.txtage.Text.ToString(), out age);
            sInfo.Age = age;
            sInfo.Grade = this.txtx.Text.ToString();
            if(!string.IsNullOrWhiteSpace(this.txty.Text.ToString()))
            {

                sInfo.StudyTime = Convert.ToDateTime(this.txty.Text.ToString());
            }
            


            this.DataGrid1.DataSource = new ManageService().GetSearchStudent(sInfo);
            this.DataGrid1.DataBind();
          
        }
        #endregion




        #region Web 窗体设计器生成的代码
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: 该调用是 ASP.NET Web 窗体设计器所必需的。
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// 设计器支持所需的方法 - 不要使用代码编辑器修改
        /// 此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            this.DataGrid1.PageIndexChanged += new System.Web.UI.WebControls.DataGridPageChangedEventHandler(this.DataGrid1_PageIndexChanged);
            this.Load += new System.EventHandler(this.Page_Load);

        }
        #endregion

        private void Button1_Click(object sender, System.EventArgs e)
        {
            GetStuData();
        }

        private void DataGrid1_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
        {
            this.DataGrid1.CurrentPageIndex = e.NewPageIndex;
            GetStuData();
        }
    }
}