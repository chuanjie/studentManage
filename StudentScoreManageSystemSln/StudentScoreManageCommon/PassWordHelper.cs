﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace StudentScoreManageCommon
{

    public class PassWordHelper
    {
        const int MD5_RIGHT_SHIFT_NUMBER = 10;//md5右移的位数
        const int MD5_ADD_NUMBER = 7;        //md5每个字母ASCII加的数字
        const int SHA1_RIGHT_SHIFT_NUMBER = 15; //sha1右移的位数
        const int SHA1_ADD_NUMBER1 = 36;        //sha1前半部每个字母ASCII加的数字,最大加到126(,)
        const int SHA1_ADD_NUMBER2 = 6;        //sha1后半部分每个字母ASCII加的数字,最大加到96(')

        /// <summary>
        /// 明文密码 加强
        /// </summary>
        /// <param name="password">明文密码</param>
        /// <returns></returns>
        public static string PasswordToStrong(string password)
        {
            //1、first md5
            string md5Password = CreateMd5(password, Encoding.UTF8);
            return Md5PasswordToStrong(md5Password);
        }

        /// <summary>
        /// md5密码 加强
        /// </summary>
        /// <param name="md5Password">md5密码</param>
        /// <returns></returns>
        public static string Md5PasswordToStrong(string md5Password)
        {
            //如果长度不等于32位，则不做加强处理
            if (md5Password.Length != 32)
                return md5Password;
            md5Password = md5Password.ToUpper();//转大写
            //2.1 右移N位
            string newPassword = StringRightShift(md5Password, MD5_RIGHT_SHIFT_NUMBER);
            //2.2 每位都加M
            newPassword = StringAddNumber(newPassword, MD5_ADD_NUMBER);
            //2.3 在第六位加上一个字符串
            newPassword = newPassword.Insert(6, "wElc0me2012");
            //3 sha1
            newPassword = EncryptToSha1(newPassword, Encoding.UTF8);//utf8编码
            //4.1 右移N位
            newPassword = StringRightShift(newPassword, SHA1_RIGHT_SHIFT_NUMBER);
            //4.2 前11位加一个数字，后29位加一个数字
            string firstPart = newPassword.Substring(0, 11);
            firstPart = StringAddNumber(firstPart, SHA1_ADD_NUMBER1); //前11位加一个数字
            string secondPart = newPassword.Substring(11);
            secondPart = StringAddNumber(secondPart, SHA1_ADD_NUMBER2);//后29位加另外一个数字
            return string.Concat(firstPart, secondPart);
        }

        /// <summary>
        /// MD5
        /// </summary>
        /// <param name="text">加密的明文</param>
        /// <param name="encode">编码</param>
        /// <returns></returns>
        private static string CreateMd5(string text, Encoding encode)
        {
            MD5 md5 = MD5.Create();
            byte[] s = md5.ComputeHash(encode.GetBytes(text));
            return ArrayToString(s);
        }

        /// <summary>
        /// 字符串右移
        /// </summary>
        /// <param name="str"></param>
        /// <param name="num">移动的位数</param>
        /// <returns></returns>
        private static string StringRightShift(string str, int num)
        {
            string leaveString = str.Substring(num);
            string shiftString = str.Substring(0, num);
            return string.Concat(leaveString, shiftString);
        }

        /// <summary>
        /// 字符串每位加数字
        /// </summary>
        /// <param name="str"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        private static string StringAddNumber(string str, int num)
        {
            var sb = new StringBuilder();
            foreach (char t in str)
            {
                int intFont = (int)t + num;
                sb.Append(Convert.ToChar(intFont));
            }
            return sb.ToString();
        }

        /// <summary>
        /// sha1(自定义编码)
        /// </summary>
        /// <param name="text">需要加密的字符串</param>
        /// <param name="encode">编码</param>
        /// <returns></returns>
        private static string EncryptToSha1(string text, Encoding encode)
        {
            byte[] data = encode.GetBytes(text);
            byte[] encryptData;
            using (SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider())
            {
                encryptData = sha1.ComputeHash(data);
                return ArrayToString(encryptData);
            }
        }

        /// <summary>
        /// Array转换为string
        /// </summary>
        /// <param name="bytes">要转换的array</param>
        /// <returns></returns>
        private static string ArrayToString(byte[] bytes)
        {
            StringBuilder enText = new StringBuilder();
            foreach (byte iByte in bytes)
            {
                enText.AppendFormat("{0:X2}", iByte);
            }
            return enText.ToString();
        }

    }
}
