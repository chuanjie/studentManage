﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentScoreManageEntity
{
    public class StudentInfo
    {
        //学号, 姓名, 性别, 年龄, 所在院系, 班级名, 入学年份

        public string StudentNumber { get; set; }

        public string StudentName { get; set; }

        public string Sex { get; set; }

        public int Age { get; set; }

        public string Grade { get; set; }

        public string Class { get; set; }

        public DateTime StudyTime { get; set; }
    }
}
