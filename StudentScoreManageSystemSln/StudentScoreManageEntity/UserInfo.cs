﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentScoreManageEntity
{
    public class UserInfo
    {
        //username,password,[group]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string group { get; set; }
    }
}
