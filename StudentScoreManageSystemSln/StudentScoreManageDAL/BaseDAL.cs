﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentScoreManageDAL
{
    public class BaseDAL
    {

        public SqlConnection Context()
        {
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationSettings.AppSettings["GradeSys"]);
            return conn;
        }
    }
}
