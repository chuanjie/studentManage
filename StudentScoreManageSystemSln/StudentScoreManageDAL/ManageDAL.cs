﻿using StudentScoreManageCommon;
using StudentScoreManageEntity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentScoreManageDAL
{
    
    public class ManageDAL
    {
        private SqlConnection conn = new SqlConnection(BaseConfig.ConnectionString);

        #region 取得课程数据
        public DataTable GetCourseList()
        {
            string sql = string.Format("select 课程号, 课程名, 先修课 from 课程表");
            return GetDataTableBysql(sql);
        }
        #endregion

        public DataTable GetGradeList()
        {
            string sql = string.Format("select 成绩表.学号,姓名,课程号,成绩,性别,班级名  from 成绩表 join 学生信息表 on 成绩表.学号 = 学生信息表.学号");
            return GetDataTableBysql(sql);
        }

        #region ExeSql
        public int ExeSql(string sql)
        {
            int retData = 0;
            SqlCommand cmd = new SqlCommand(sql, conn);
            try
            {
                conn.Open();
                retData = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
            finally
            {
                conn.Close();
            }
            return retData;
        }
        public DataTable GetDataTableBysql(string sql)
        {
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataSet ds = new DataSet();
            DataTable dt = null;
            try
            {
                da.Fill(ds, "t");
                dt = ds.Tables["t"];
            }
            catch (Exception ex)
            {

            }
            return dt;
        }
        #endregion
        /// <summary>
        /// 获取班级信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetClassName()
        {
            string sql = string.Format("select 班级名 from 学生信息表 group by 班级名");
            return GetDataTableBysql(sql);
        }
        /// <summary>
        /// 根据班级名称获取学生信息
        /// </summary>
        /// <param name="className"></param>
        /// <returns></returns>
        public DataTable GetStudentByClassName(string className)
        {
            string sql = string.Format("select 学号,姓名,性别,班级名,所在院系 from 学生信息表 where 班级名='{0}'",className);
            return GetDataTableBysql(sql);
        }

       

        public DataTable GetAdminUrl()
        {
            string sql = string.Format("select Id, Url, UrlName, Comment from AdminUrl");
            return GetDataTableBysql(sql);
        }

        public DataTable GetGroupList()
        {
            string sql = string.Empty;
            sql = string.Format("select id,[Group] from AdminGroup order by id asc");
            return GetDataTableBysql(sql);
        }

        public DataTable GetStudent()
        {
            string sql = string.Format("select 学号, 姓名, 性别, 年龄, 所在院系, 班级名, 入学年份 from 学生信息表");
            return GetDataTableBysql(sql);
        }

        public DataTable GetSearchStudent(StudentInfo model)
        {
            string sql = string.Empty;
            ArrayList sqlArr = new ArrayList();
            if (!string.IsNullOrWhiteSpace(model.StudentNumber))
            {
                sqlArr.Add(string.Format("学号 like '%{0}%' ", model.StudentNumber));
            }
            if (!string.IsNullOrWhiteSpace(model.StudentName))
            {
                sqlArr.Add(string.Format("姓名 like '%{0}%' ", model.StudentName));
            }
            if (model.Age>0)
            {
                sqlArr.Add(string.Format("年龄 like '%{0}%' ", model.Age.ToString()));
            }
            if (!string.IsNullOrWhiteSpace(model.Grade))
            {
                sqlArr.Add(string.Format("所有院系 like '%{0}%' ", model.Grade));
            }
            if (model.StudyTime.Year>1970)
            {
                sqlArr.Add(string.Format("入学年份 like '%{0}%' ", model.StudyTime.ToString()));
            }
            foreach (string str in sqlArr)
            {
                sql = sql + str + " and ";

            }
            sql = "select * from 学生信息表 where " + sql;
            int i = sql.Trim().Length - 5;
            sql = sql.Substring(0, i);
            return GetDataTableBysql(sql);
        }

        public DataTable GetStudentStatis()
        {
            string sql = string.Format("select 课程号,sum(成绩) as 总分,avg(成绩) as 平均分,count(学号) as 人数 from v_stuGrade group by 课程号");
            return GetDataTableBysql(sql);
        }
        public DataTable GetTeacherCourse()
        {
            string sql  = string.Format("select 教师名, 课程号, 学时数, 班级名 from 授课表");
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            return GetDataTableBysql(sql);
        }

        //public DataTable GetAdminUrl()
        //{
        //    string sql = string.Format("select Id, Url, UrlName, Comment from AdminUrl");
        //    return GetDataTableBysql(sql);
        //}
    }
}
