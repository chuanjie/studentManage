﻿using StudentScoreManageCommon;
using StudentScoreManageEntity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentScoreManageDAL
{
    public class UserInfoDAL: BaseDAL
    {
        private SqlConnection conn = new SqlConnection(BaseConfig.ConnectionString);
        public UserInfo GetUserByUserName(string userName)
        {
            string sql = string.Empty;
           
            sql = string.Format("select username,password,[group],userid from admin where username='{0}'", userName);
            SqlDataReader dr;
            SqlCommand cmd = new SqlCommand(sql, conn);

            var userInfo = new UserInfo();
            try
            {
                conn.Open();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    //Session["username"] = dr["username"].ToString();
                    //Session["group"] = dr["group"].ToString();

                    userInfo.UserName = dr["username"].ToString();
                    userInfo.group= dr["group"].ToString();
                    userInfo.UserId = Convert.ToInt32(dr["userid"]);
                    userInfo.PassWord = dr["password"].ToString();
                    //Response.Redirect("Main.aspx");
                }
                
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
            finally
            {
                Context().Close();
            }
            return userInfo;
        }
    }
}
